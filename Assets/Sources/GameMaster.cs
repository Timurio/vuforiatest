﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using VuforiaTest.PlayerController;

namespace VuforiaTest.Gameplay
{
    public class GameMaster : MonoBehaviour, ITrackableEventHandler
    {
        public static GameMaster Instance { get; private set; }

        [SerializeField]
        GameObject findTargetScreen;
        [SerializeField]
        GameObject gameplayScreen;
        [SerializeField]
        GameObject particleSystem;

        public TrackableBehaviour trackableBehaviour;

        private void Awake()
        {
            if (Instance)
                DestroyImmediate(gameObject);
            else
                Instance = this;
        }

        private void Start()
        {
            Application.targetFrameRate = 30;

            if (!trackableBehaviour)
                trackableBehaviour = GetComponent<TrackableBehaviour>();
            trackableBehaviour.RegisterTrackableEventHandler(this);
        }

        public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
                ShowGameplayScreen();
            else 
                ShowFindTargetScreen();
        }

        void ShowFindTargetScreen()
        {
            findTargetScreen.SetActive(true);
            gameplayScreen.SetActive(false);
            particleSystem.SetActive(false);
            AudioListener.volume = 0.3f;
            Player.PlayerInstance.PlayerMovement.ResetPosition();
        }

        void ShowGameplayScreen()
        {
            findTargetScreen.SetActive(false);
            gameplayScreen.SetActive(true);
            particleSystem.SetActive(true);
            AudioListener.volume = 1f;
            AudioMaster.Instance.PlayFindTargetClip();
            Player.PlayerInstance.PlayerMovement.Invoke("ShowPlayer", 0.5f);
        }
    }
}