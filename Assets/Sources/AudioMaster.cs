﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VuforiaTest.Gameplay
{
    [DefaultExecutionOrder(-100)]
    public class AudioMaster : MonoBehaviour
    {
        public static AudioMaster Instance { get; private set; }

        [SerializeField]
        AudioClip musicClip;
        [SerializeField]
        AudioClip findTargetClip;
        [SerializeField]
        AudioClip meowClip;
        [SerializeField]
        AudioClip itchClip;


        private void Awake()
        {
            if (Instance)
                DestroyImmediate(gameObject);
            else
                Instance = this;
        }

        private void Start()
        {
            PlayMusic();
        }

        void PlayMusic()
        {
            AudioSource musicAS = gameObject.AddComponent<AudioSource>();
            musicAS.clip = musicClip;
            musicAS.volume = 0.5f;
            musicAS.loop = true;
            musicAS.Play();
        }

        void PlayOneShot(AudioClip clip)
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.PlayOneShot(clip, 1f);
            Destroy(audioSource, clip.length + 0.1f);
        }

        public void PlayFindTargetClip()
        {
            PlayOneShot(findTargetClip);
        }        
        public void PlayMeowClip()
        {
            PlayOneShot(meowClip);
        }
        public void PlayItchClip()
        {
            PlayOneShot(itchClip);
        }        
    }
}