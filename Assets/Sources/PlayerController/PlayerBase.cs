﻿using UnityEngine;
using VuforiaTest.UtilityTools;

namespace VuforiaTest.PlayerController
{
    public class PlayerBase : MonoBehaviour
    {
        Player player;
        public Player Player
        {
            get
            {
                return Utils.GetCachedComponent(gameObject, ref player);
            }
        }

        PlayerControls playerControls;
        public PlayerControls PlayerControls
        {
            get
            {
                return Utils.GetCachedComponent(gameObject, ref playerControls);
            }
        }

        PlayerMovement playerMovement;
        public PlayerMovement PlayerMovement
        {
            get
            {
                return Utils.GetCachedComponent(gameObject, ref playerMovement);
            }
        }
    }    
}