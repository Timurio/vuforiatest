﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace VuforiaTest.PlayerController
{
    public class PlayerControls : PlayerBase
    {
        [SerializeField]
        string sitStandButtonName = "SitStandButton";
        [SerializeField]
        string meowButtonName = "MeowButton";
        [SerializeField]
        string itchingButtonName = "ItchButton";
        [SerializeField]
        string horizontalAxisName = "Horizontal";
        [SerializeField]
        string verticalAxisName = "Vertical";

        public Vector2 MovementInput
        {
            get
            {
                return new Vector2(CrossPlatformInputManager.GetAxis(horizontalAxisName), CrossPlatformInputManager.GetAxis(verticalAxisName));
            }
        }

        public bool SitStandButtonUp
        {
            get
            {
                return CrossPlatformInputManager.GetButtonUp(sitStandButtonName);
            }
        }

        public bool MeowButtonUp
        {
            get
            {
                return CrossPlatformInputManager.GetButtonUp(meowButtonName);
            }
        }

        public bool ItchingButtonUp
        {
            get
            {
                return CrossPlatformInputManager.GetButtonUp(itchingButtonName);
            }
        }
    }
}