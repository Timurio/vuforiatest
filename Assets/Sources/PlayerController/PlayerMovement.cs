﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VuforiaTest.Gameplay;

namespace VuforiaTest.PlayerController
{
    public class PlayerMovement : PlayerBase
    {
        public bool IsSitting { get; private set; }
        public bool IsItching { get; private set; }
        public bool IsMeowing { get; private set; }

        [SerializeField]
        float RotationSpeed;
        [SerializeField]
        float MovementSpeed;

        Animator anim;
        Transform targetTransform;

        #region States
        bool canMove
        {
            get
            {
                return !(IsItching || IsMeowing || IsSitting);
            }
        }

        bool canSit
        {
            get
            {
                return !(IsItching || IsMeowing);
            }
        }  

        bool canMeow
        {
            get
            {
                return !IsItching;
            }
        }

        bool canItch
        {
            get
            {
                return !IsMeowing;
            }
        }
        #endregion /States

        private void Update()
        {
            if (!anim)
                anim = GetComponent<Animator>();
            if (!targetTransform || targetTransform != GameMaster.Instance.trackableBehaviour.transform)
                targetTransform = GameMaster.Instance.trackableBehaviour.transform;

            if (PlayerControls.SitStandButtonUp)
            {                
                if (IsSitting)
                    StandUp();
                else
                    SitDown();
            }

            if (PlayerControls.MeowButtonUp)
                StartMeow();

            if (PlayerControls.ItchingButtonUp)
                StartItching();

            MovePlayer();            
        }

        void MovePlayer()
        {
            if (!canMove)
                return;

            Vector3 playerMovement = new Vector3(PlayerControls.MovementInput.x, 0f, PlayerControls.MovementInput.y);
            if (playerMovement.magnitude <= 0.1f)
            {
                anim.SetFloat("Speed", 0f);
                return;
            }

            Vector3 targetForward = targetTransform.InverseTransformDirection(Camera.main.transform.forward);
            targetForward.y = 0;           

            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(playerMovement) * Quaternion.LookRotation(targetForward), RotationSpeed * Time.deltaTime);
            transform.position = Vector3.Lerp(transform.position, transform.position + transform.forward * playerMovement.magnitude * MovementSpeed, Time.deltaTime);

            anim.SetFloat("Speed", playerMovement.magnitude);
        }

        public void ResetPosition()
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            gameObject.SetActive(false);
        }

        public void ShowPlayer()
        {
            gameObject.SetActive(true);
        }

        public void SitDown()
        {
            if (!canSit)
                return;

            IsSitting = true;
            anim.SetBool("Sit", true);
        }

        public void StandUp()
        {
            anim.SetBool("Sit", false);            
        }

        public void StoodUp()
        {
            IsSitting = false;
            anim.SetBool("Sit", false);
        }

        public void StartItching()
        {
            if (!canItch)
                return;

            IsItching = true;
            anim.SetTrigger("Itching");
            AudioMaster.Instance.PlayItchClip();
        }

        public void StopItching()
        {
            IsItching = false;
        }

        public void StartMeow()
        {
            if (!canMeow)
                return;

            IsMeowing = true;
            anim.SetTrigger("Meow");
            AudioMaster.Instance.PlayMeowClip();
        }

        public void StopMeow()
        {
            IsMeowing = false;
        }

        float SignedAngle(Vector3 a, Vector3 b)
        {
            float angle = Vector3.Angle(a, b) * Mathf.Sign(Vector3.Cross(a, b).y);

            return angle;
        }
    }
}