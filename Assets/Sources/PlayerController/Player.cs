﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VuforiaTest.PlayerController
{
    public class Player : PlayerBase
    {
        public static Player PlayerInstance { get; private set; }

        private void Awake()
        {
            if (PlayerInstance)
                DestroyImmediate(gameObject);
            else
                PlayerInstance = this;
        }
    }
}