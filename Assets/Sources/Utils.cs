﻿using UnityEngine;

namespace VuforiaTest.UtilityTools
{
    public static class Utils
    {
        public static T GetCachedComponent<T>(GameObject gameObject, ref T cachedComponent) where T : MonoBehaviour
        {
            if (cachedComponent == null)
            {
                cachedComponent = gameObject.GetComponent<T>();
            }

            return cachedComponent;
        }
    }
}