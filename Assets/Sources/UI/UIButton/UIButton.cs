﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

namespace VuforiaTest.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UIButton : MonoBehaviour
    {
        [SerializeField]
        protected string buttonAxesName;

        CanvasGroup canvasGroup;

        private void Awake()
        {
            GetComponent<Button>().onClick.AddListener(() => OnClick());
        }

        protected virtual void OnClick()
        {
            CrossPlatformInputManager.SetButtonUp(buttonAxesName);
            StartCoroutine(HideAndShowButton());
        }

        IEnumerator HideAndShowButton()
        {
            if (!canvasGroup)
                canvasGroup = GetComponent<CanvasGroup>();

            canvasGroup.alpha = 0f;
            canvasGroup.blocksRaycasts = false;

            yield return new WaitForSeconds(1f);

            while (canvasGroup.alpha < 0.99f)
            {
                yield return new WaitForSeconds(0.02f);
                canvasGroup.alpha += 0.02f;
            }

            canvasGroup.alpha = 1f;
            canvasGroup.blocksRaycasts = true;
        }
    }
}