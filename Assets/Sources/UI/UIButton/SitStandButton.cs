﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VuforiaTest.PlayerController;

namespace VuforiaTest.UI
{
    public class SitStandButton : UIButton
    {
        [SerializeField]
        string standText;
        [SerializeField]
        string sitText;

        [SerializeField]
        Text buttonText;

        bool IsSitting;

        private void Update()
        {
            if (IsSitting != Player.PlayerInstance.PlayerMovement.IsSitting)
            {
                IsSitting = !IsSitting;
                buttonText.text = IsSitting ? standText : sitText;
            }
        }        
    }
}