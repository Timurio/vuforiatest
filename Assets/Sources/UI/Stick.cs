﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;
using VirtualAxis = UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager.VirtualAxis;

namespace VuforiaTest.UI
{
    [RequireComponent(typeof(NonDrawingGraphic))]
    public class Stick : MonoBehaviour, IPointerDownHandler, IDragHandler, IEndDragHandler, IPointerUpHandler
    {
        [SerializeField]
        Transform stickTransform;
        [SerializeField]
        Transform stickBaseTransform;

        [SerializeField]
        float minSens = 0.1f;

        [Header("Stick axes names")]
        [SerializeField]
        string horizontalAxisName = "Horizontal";
        [SerializeField]
        string verticalAxisName = "Vertical";

        [Header("Sticks size in mm")]
        [SerializeField]
        float stickBaseRealSize = 35f;

        VirtualAxis horizontalVirtualAxis;
        VirtualAxis verticalVirtualAxis;

        float maxMagnitude;

        private void Awake()
        {
            CrossPlatformInputManager.UnRegisterVirtualAxis(horizontalAxisName);
            CrossPlatformInputManager.UnRegisterVirtualAxis(verticalAxisName);

            ApplyRealStickSize();
        }

        private void OnEnable()
        {
            UpdateVirtualAxes(Vector2.zero);
        }

        void CreateVirtualAxes()
        {
            horizontalVirtualAxis = new VirtualAxis(horizontalAxisName);
            CrossPlatformInputManager.RegisterVirtualAxis(horizontalVirtualAxis);

            verticalVirtualAxis = new VirtualAxis(verticalAxisName);
            CrossPlatformInputManager.RegisterVirtualAxis(verticalVirtualAxis);            

            UpdateVirtualAxes(Vector2.zero);
        }

        void UpdateVirtualAxes(Vector2 value)
        {
            if (horizontalVirtualAxis == null || verticalVirtualAxis == null)
                CreateVirtualAxes();

            ApplySens(ref value);

            horizontalVirtualAxis.Update(value.x);
            verticalVirtualAxis.Update(value.y);            
        }

        void ApplySens(ref Vector2 value)
        {
            if (value.magnitude < minSens)
                value = Vector2.zero;
        }

        void ApplyRealStickSize()
        {
            float realSize = (stickBaseTransform.GetComponent<RectTransform>().sizeDelta.x * stickBaseTransform.lossyScale.x * 25.4f) / Screen.dpi;
            float mult = stickBaseRealSize / realSize;

            stickBaseTransform.GetComponent<RectTransform>().sizeDelta *= mult;
            stickTransform.GetComponent<RectTransform>().sizeDelta *= mult;

            maxMagnitude = (stickBaseTransform.GetComponent<RectTransform>().sizeDelta.x * stickBaseTransform.lossyScale.x) / 4f;
        }

        public void ClearAxes()
        {
            UpdateVirtualAxes(Vector2.zero);
            stickTransform.gameObject.SetActive(false);
            stickBaseTransform.gameObject.SetActive(false);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            stickBaseTransform.gameObject.SetActive(true);
            stickTransform.gameObject.SetActive(true);

            stickTransform.position = eventData.position;
            stickBaseTransform.position = eventData.position;
        }

        public void OnDrag(PointerEventData eventData)
        {
            Vector2 deltaRaw = eventData.position - eventData.pressPosition;

            if (deltaRaw.magnitude > maxMagnitude)
                deltaRaw = Vector2.ClampMagnitude(deltaRaw, maxMagnitude);

            float analogSens = deltaRaw.magnitude / maxMagnitude;
            stickTransform.position = eventData.pressPosition + deltaRaw;

            if (deltaRaw.magnitude > 0f)
                UpdateVirtualAxes(deltaRaw.normalized * analogSens);
            else
                UpdateVirtualAxes(Vector2.zero);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            UpdateVirtualAxes(Vector2.zero);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            ClearAxes();
        }
    }
}