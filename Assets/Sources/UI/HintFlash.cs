﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace VuforiaTest.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class HintFlash : MonoBehaviour, IPointerDownHandler
    {
        public float speed = 5f;

        CanvasGroup canvasGroup;
        bool hide = false;

        private void Start()
        {
            StartCoroutine(Flashing());
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            hide = true;
        }
        
        IEnumerator Flashing()
        {
            canvasGroup = GetComponent<CanvasGroup>();

            float mult = -1f;
            canvasGroup.alpha = 1f;

            while (!hide)
            {
                yield return new WaitForSeconds(1 / speed);
                canvasGroup.alpha += 0.05f * mult;

                if (mult > 0 && canvasGroup.alpha >= 0.99f)
                    mult = -1f;
                else if (mult < 0 && canvasGroup.alpha <= 0.01f)
                    mult = 1f;
            }

            canvasGroup.alpha = 1f;
        }
    }
}